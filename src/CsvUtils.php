<?php


namespace CodeUtils;

class CsvUtils
{
    public static function mapHeader($row)
    {
        $header = [];
        $idx = 0;
        foreach ($row as $key => $value) {
            $header[StringUtils::makeSingleLine(strtolower(trim($value)))] = $idx++;
        }
        return $header;
    }
}