<?php

require "vendor/autoload.php";

print_r(
    \CodeUtils\StringUtils::format(
        "https://ho.blibli.com/aff_c?offer_id=2&aff_id=21346&url={URL:ENCODE}{QUERY_MARK:ENCODE}utm_campaign%3Dcampaign_cpc_telunjuk%26utm_source%3Daffiliates%26utm_medium%3D{affiliate_name}%26trans%3D{transaction_id}%26offer%3D{offer_id}",
        [
            "URL" => "https://blibli.com/product/123",
            "QUERY_MARK" => "?"
        ]
    )
);