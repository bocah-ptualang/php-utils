<?php

namespace CodeModels;


class Bucket
{
    private $bucket = [];
    private $batchSize = 50;
    private $fullBucketCallback;

    public function __construct($fullBucketCallback, $batchSize = 50) 
    {
        $this->batchSize = $batchSize;
        $this->fullBucketCallback = $fullBucketCallback;
    }

    public function push($data) {
        $size = 0;
        if (is_array($data)) {
            $this->bucket = array_merge($this->bucket, $data);
            $size = sizeof($data);
        } else {
            $this->bucket[] = $data;
            $size = 1;
        }
        if (sizeof($this->bucket) >= $this->batchSize) {
            //call function
            call_user_func($this->fullBucketCallback, $this->bucket);
            $this->bucket = [];
        }

        return $size;
    }

    public function forceRun(){
        $res = call_user_func($this->fullBucketCallback, $this->bucket);
        $this->bucket = [];
        return $res;
    }

    public function getCurrentSize(){
        return sizeof($this->bucket);
    }
}
