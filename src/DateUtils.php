<?php


namespace CodeUtils;


class DateUtils
{
    public const FORMAT_DB = "Y-m-d H:i:s";
    public const FORMAT_DB_DATE_ONLY = "Y-m-d";
    public const FORMAT_ELASTIC = "Y-m-d\TH:i:s\Z";
    public const FORMAT_ELASTIC_TIMEZONE = \DateTime::RFC3339;
}