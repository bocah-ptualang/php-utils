<?php

namespace CodeUtils;


class ArrayUtils
{

    public static function setValueIfExist(&$var, $property, &$value)
    {
        if (isset($value)) {
            $var[$property] = $value;
        }
    }

    public static function toArray($stdObj) {
        if (gettype($stdObj) == 'string') return json_decode($stdObj, true);
        else return json_decode(json_encode($stdObj), true);
    }

    public static function toObject($stdObj) {
        if (gettype($stdObj) == 'string') return json_decode($stdObj, false);
        else return json_decode(json_encode($stdObj), false);
    }

}
