<?php

namespace CodeUtils;


class OSUtils
{

    public static function isWindows()
    {
        return StringUtils::contains(php_uname(), "Windows");
    }
}
