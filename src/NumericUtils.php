<?php

namespace CodeUtils;


class NumericUtils
{
    public static function toInt($str, $digitSeparator = ",", $decimalSeparator = ".")
    {
        $val = preg_replace("/([^0-9\\" . $decimalSeparator . "])/i", "", $str);
        $val = str_replace($decimalSeparator, ".", $val);
        return intval($val);
    }

    public static function toFloat($str, $digitSeparator = ",", $decimalSeparator = ".")
    {
        $val = preg_replace("/([^0-9\\" . $decimalSeparator . "])/i", "", $str);
        $val = str_replace($decimalSeparator, ".", $val);
        return floatval($val);

    }
}
