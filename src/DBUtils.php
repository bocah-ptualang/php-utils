<?php


namespace CodeUtils;

class DBUtils
{

    public static function createInstance($config)
    {
        try {
            $connection = \Phalcon\Db\Adapter\Pdo\Factory::load($config);

            $eventsManager = new \Phalcon\Events\Manager();

            $eventsManager->attach('db', function ($event, $connection) {
                if ($event->getType() == 'beforeQuery') {
                    try {
                        $checkQuery = "SELECT 1";
                        if ($connection->getSQLStatement() == $checkQuery) return;
                        // the query doesnt matter, this will also fail if there's no connection
                        $connection->fetchAll($checkQuery);
                    } catch (\Exception $e) {
                        // nor we care about the specific query here, we just need to reconnect on errors
                        $connection->connect();
                    }
                }
            });

            $connection->setEventsManager($eventsManager);
            return $connection;
        } catch (\Exception $e) {
            print_r($e);
        }
    }

    public static function upsert($data, $db)
    {
        $idCondition = array_map(function ($key, $value) {
            $value = utf8_encode($value);
            return "$key = '$value'";
        }, array_keys($data[2]), $data[2]);

        $idCondition = implode(" AND ", $idCondition);

        $row = $db->fetchOne("SELECT * FROM $data[0] WHERE $idCondition LIMIT 1");


        if ($row) {
            //update
            return $db->updateAsDict($data[0], $data[1], $idCondition);
        } else {
            //insert
            unset($data[2]);
            return $db->insertAsDict($data[0], $data[1]);
        }
    }

    public static function insert($data, $db)
    {
        //insert
        return $db->insertAsDict($data[0], $data[1]);
    }

    public static function insertBulk($data, $db)
    {
        return $db->execute(self::generateInsertQuery($data[0], $data[1]));
    }

    public static function upsertBulk($data, $db)
    {
        return $db->execute(self::generateUpsertQuery($data[0], $data[1]));
    }

    public static function generateInsertQuery($table, $datas)
    {
        if (sizeof($datas) == 0) return "";

        $sql = "INSERT INTO %s (%s) VALUES %s;";

        $fields = array_map(function ($obj) {
            return "`$obj`";
        }, array_keys($datas[0]));

        $values = array_map(function ($obj) {
            return "(" .
                implode(", ", array_map(function ($d) {
                    if ($d == null) return "null";
                    //Check if field type is numeric
                    $quote = is_numeric($d) ? "" : "'";
                    return sprintf("%s%s%s", utf8_encode($quote), $d, $quote);
                }, array_values($obj)))
                . ")";
        }, $datas);

        $sql = sprintf($sql,
            $table,
            implode(", ", $fields),
            implode(",", $values)
        );


        return $sql;
    }

    public static function generateUpsertQuery($table, $datas)
    {
        if (sizeof($datas) == 0) return "";

        $sql = "INSERT INTO %s (%s) VALUES %s ON DUPLICATE KEY UPDATE %s;";

        //Fields
        $fields = array_map(function ($obj) {
            return "`$obj`";
        }, array_keys($datas[0]));

        //Values
        $values = array_map(function ($obj) {
            return "(" .
                implode(", ", array_map(function ($d) {
                    if ($d == null) return "null";
                    //Check if field type is numeric
                    $quote = is_numeric($d) ? "" : "'";
                    return sprintf("%s%s%s", utf8_encode($quote), $d, $quote);
                }, array_values($obj)))
                . ")";
        }, $datas);

        //Updates
        $updateFields = array_map(function ($obj) {
            return "`$obj`=VALUES(`$obj`)";
        }, array_keys($datas[0]));

        $sql = sprintf($sql,
            $table,
            implode(", ", $fields),
            implode(",", $values),
            implode(", ", $updateFields)
        );


        return $sql;
    }


}