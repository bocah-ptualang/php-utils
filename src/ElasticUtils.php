<?php

namespace CodeUtils;


class ElasticUtils
{

    public static function buildBulkData($data, $index, $type = null)
    {
        //Build x-ndjson
        $bulkBody = [];
        foreach ($data as $key => $value) {
            $head = ['index' => ['_index' => $index]];

            if ($type != null) {
                $head['index']['_type'] = $type;
            }

            if (isset($value->_id)) {
                $head['index']['_id'] = $value->_id;
                unset($value->_id);
            }else if (isset($value->id)) {
                $head['index']['_id'] = $value->id;
                unset($value->id);
            }

            $bulkBody[] = $head;
            $bulkBody[] = $value;
        }

        return $bulkBody;
    }

    public static function buildBulkUpdateData($data, $index, $type = null)
    {
        //Build x-ndjson
        $bulkBody = [];
        foreach ($data as $key => $value) {
            $head = ['update' => ['_index' => $index]];

            if ($type != null) {
                $head['update']['_type'] = $type;
            }

            $head['update']['_id'] = $value->_id;
            unset($value->_id);
            $doc = ['doc' => $value];

            $bulkBody[] = $head;
            $bulkBody[] = json_decode(json_encode($doc), true);
        }

        return $bulkBody;
    }

    public static function buildDslBody($dsl)
    {
        if (!isset($dsl['body'])) return $dsl;
        if (is_array($dsl['body'])) return $dsl;

        $dsl['body'] = json_encode($dsl['body']);
        return $dsl;
    }

    public static function buildScriptSource($data) {
        $template = "ctx._source['%s'] = %s%s%s";
        $sources = [];
        foreach ($data as $key => $value) {
            $quote = is_numeric($value) ? "" : "'";
            $sources[] = sprintf($template, $key, $quote, $value, $quote);
        }
        return implode("; ",$sources);
    }

}
