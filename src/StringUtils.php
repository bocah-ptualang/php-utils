<?php

namespace CodeUtils;


class StringUtils
{

    public static function getValue(&$var, $default = null)
    {
        return (isset($var) && $var != null) ? $var : $default;
    }

    public static function setDefault(&$var, $value) {
        if (isset($var)) return;
        $var = $value;
    }

    public static function setValue(&$model, &$value, $default = null) {
        $model = isset($value) ? $value : $default;
    }

    public static function format($format, $values)
    {
        preg_match_all("/{(\b[A-Z_]+)[:]?(.*?)}/", $format, $groups);

        if (sizeof($groups[0]) > 0) {
            for ($i = 0; $i < sizeof($groups[0]); $i++) {
                $key = $groups[1][$i];
                $function = $groups[2][$i];
                $value = isset($values[$key]) ? $values[$key] : null;

                if ($value != null) {
                    switch ($function) {
                        case 'ENCODE':
                            $value = urlencode($value);
                    }

                    $format = str_replace($groups[0][$i], $value, $format);
                }
            }
        }

        return $format;
    }

    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public static function contains($string, $search)
    {
        if ($search == null || $search == '') return false;
        return strpos($string, $search) !== false;
    }

    public static function encode($string, $excepts = null, $regex = false)
    {
        $restore = [];
        if ($excepts != null) {
            $exceptValues = $excepts;
            if ($regex) {
                preg_match_all("$excepts", $string, $groups);
                $exceptValues = $groups[0];
            }

            for ($i = 0; $i < sizeof($exceptValues); $i++) {
                $value = $exceptValues[$i];
                $restore["PARAM$i"] = $value;
                $string = str_replace("$value", "PARAM$i", $string);
            }
        }

        $encoded = urlencode($string);

        foreach ($restore as $key => $value) {
            $encoded = str_replace($key, $value, $encoded);
        }

        return $encoded;
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public static function camelize($input, $separator = "-")
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }

    public static function makeSingleLine($text)
    {
        $text = str_replace("\r\n", "", $text);
        $text = str_replace("\n", "", $text);
        return $text;
    }

    public static function ellipsis($text, $length, $fixLength = false)
    {
        $el = mb_strimwidth($text, 0, $length, "...");
        if ($fixLength) {
            $el = str_pad($el, $length, " ");
        }
        return $el;
    }

    public static function regexSearch($text, $pattern, $index = null)
    {
        $matches = null;
        preg_match($pattern, $text, $matches);
        if ($index == null) return $matches;
        if (sizeof($matches) == 0) return null;
        return $matches[$index];
    }

    public static function trim($text) {
        return trim(preg_replace("/(\s)\s+/","$1", $text));
    }
}
